/* 
 * Copyright (C) 1998-2001 Free Software Foundation
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this library; if not, write to the Free
 *  Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors : Vadim Strizhevsky
 *           Eskil Heyn Olsen
 */


#include <config.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>
#include <panel-applet-gconf.h>

#include <signal.h>

#include "gpilot-daemon.h"
#include "gpilot-applet-progress.h"
#include "gnome-pilot-client.h"

enum {
  TARGET_URI_LIST,
};

typedef struct _pilot_properties pilot_properties;

struct _pilot_properties {
	GList* pilot_ids;
	GList* cradles;
	gchar * exec_when_clicked;
	gboolean popups;
};

typedef struct {
	gchar *cradle;
        gchar *backupdir;
	gchar *username;
	gint id;
} restore_properties;

typedef enum { INITIALISING, PAUSED, CONNECTING_TO_DAEMON, SYNCING, WAITING, NUM_STATES } state;


char *pixmaps[] = 
{
    GNOME_ICONDIR "/sync_broken.png",
    GNOME_ICONDIR "/sync_paused.png",
    GNOME_ICONDIR "/sync_icon.png",
    GNOME_ICONDIR "/syncing_icon.png",
    GNOME_ICONDIR "/sync_icon.png",
};


typedef struct {
	PanelApplet *applet;
	pilot_properties properties; /* = { NULL }; */
	state curstate;
	GtkWidget *image; 
	GtkWidget *dialogWindow; 
	GtkWidget *operationDialogWindow;
	GtkWidget *pb;
	GtkTextBuffer *message_buffer;
	GPilotAppletProgress *c_progress;

	guint timeout_handler_id;
	gboolean double_clicked;
	gboolean druid_already_launched;

	GtkWidget *progressDialog;
	GtkWidget *sync_label;
	GtkWidget *overall_progress_bar;
	GtkWidget *conduit_progress_bar;
	GtkWidget *message_area;
	GtkWidget *cancel_button;
	GtkWidget *chooseDialog; 
	GtkWidget *restoreDialog;
	GdkColor  errorColor;
	gchar    *ui_file;

	GnomePilotClient *gpc;
} PilotApplet;

#define PILOT_APPLET(x) ((PilotApplet*)(x))

static void show_dialog (PilotApplet *self, GtkMessageType type, gchar*,...);
static void cancel_cb (GtkButton* button, gpointer whatever);

static void save_properties (PilotApplet *self);
static void load_properties (PilotApplet *self);

static void pilot_draw (PilotApplet*);

static void pilot_execute_program (PilotApplet*, const gchar *);
static gchar *pick_pilot (PilotApplet *self);
static void response_cb (GtkDialog *dialog, gint id, gpointer data);

static gboolean timeout (PilotApplet *self);

#define GPILOTD_DRUID "gpilotd-control-applet --assistant"
#define GPILOTD_CAPPLET "gpilotd-control-applet"
#define CONDUIT_CAPPLET "gpilotd-control-applet --cap-id=1"

/******************************************************************/

static GtkBuilder *
load_ui (const gchar *filename, const gchar *widget)
{
	GtkBuilder *ui;
	gchar *objects[2] = {NULL, NULL};

	ui = gtk_builder_new ();
	objects[0] = (gchar *)widget;
	gtk_builder_add_objects_from_file (ui, filename, objects, NULL);

	return ui;
}

static GtkWidget *
get_widget (GtkBuilder *ui, const gchar *name)
{
	return GTK_WIDGET (gtk_builder_get_object (ui, name));
}

static void 
gpilotd_connect_cb (GnomePilotClient *client, 
		    const gchar *id,
		    const GNOME_Pilot_UserInfo *user,
		    gpointer user_data)
{
	gchar *buf;
	GError *error = NULL;
	PilotApplet *applet = PILOT_APPLET (user_data);

	gtk_widget_set_tooltip_text(GTK_WIDGET(applet->applet), ("Synchronizing..."));
  
	if (!gtk_widget_get_realized (GTK_WIDGET (applet->image))) {
		g_warning ("! realized");
		return;
	}
	applet->curstate = SYNCING;
	g_message ("state = SYNCING");

	pilot_draw (applet);

	if (applet->properties.popups == FALSE) return;

	if (applet->progressDialog == NULL) {
		gtk_window_set_default_icon_from_file (
		    GNOME_ICONDIR "/sync_icon.png", &error);
		GtkBuilder *ui               = load_ui (applet->ui_file,"ProgressDialog");
		applet->progressDialog       = get_widget (ui,"ProgressDialog");
		applet->sync_label           = get_widget (ui,"sync_label");
		applet->message_area         = get_widget (ui,"message_area");
		applet->overall_progress_bar = get_widget (ui,"overall_progress_bar");
		applet->conduit_progress_bar = get_widget (ui,"conduit_progress_bar");
		applet->cancel_button        = get_widget (ui,"cancel_button");
		applet->message_buffer       = gtk_text_view_get_buffer(
				GTK_TEXT_VIEW(applet->message_area));

		g_signal_connect   (G_OBJECT (applet->cancel_button),"clicked",
				   G_CALLBACK (cancel_cb),applet);
	} else {
		gtk_text_buffer_set_text (applet->message_buffer, "", -1);
	}

	gtk_widget_set_sensitive (applet->cancel_button, FALSE);
	buf=g_strdup_printf (_("%s Synchronizing"),id);
	gtk_label_set_text (GTK_LABEL (applet->sync_label),buf);
	g_free (buf);
	gtk_widget_show_all (applet->progressDialog);

	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (applet->overall_progress_bar), _("Connecting..."));
	gtk_progress_bar_set_text (GTK_PROGRESS_BAR (applet->conduit_progress_bar), "");
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->overall_progress_bar),0);
	gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->conduit_progress_bar),0);

	gpilot_applet_progress_set_progress (applet->c_progress, GTK_PROGRESS_BAR (
		applet->conduit_progress_bar));
	gpilot_applet_progress_start (applet->c_progress);

	gdk_color_parse ("red",&(applet->errorColor));
}

static void 
gpilotd_disconnect_cb (GnomePilotClient *client, 
		       const gchar *id,
		       gpointer user_data)
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	gtk_widget_set_tooltip_text(GTK_WIDGET(applet->applet), _("Ready to synchronize"));

	applet->curstate = WAITING;
	g_message ("state = READY");

	pilot_draw (applet);
	if (applet->properties.popups && applet->progressDialog!=NULL) {
		gpilot_applet_progress_stop (applet->c_progress);
		gtk_widget_hide (applet->progressDialog);
	}
}

/* taken from http://live.gnome.org/GnomeLove/PanelAppletTutorial */
/* static void */
/* applet_back_change (PanelApplet			*a, */
/* 		    PanelAppletBackgroundType	type, */
/* 		    GdkColor			*color, */
/* 		    GdkPixmap			*pixmap, */
/* 		    PilotApplet			*applet)  */
/* { */
/*         GtkRcStyle *rc_style; */
/*         GtkStyle *style; */

/*         /\* reset style *\/ */
/*         gtk_widget_set_style (GTK_WIDGET (applet->applet), NULL); */
/*         rc_style = gtk_rc_style_new (); */
/*         gtk_widget_modify_style (GTK_WIDGET (applet->applet), rc_style); */
/*         g_object_unref (rc_style); */

/*         switch (type) { */
/*                 case PANEL_COLOR_BACKGROUND: */
/*                         gtk_widget_modify_bg (GTK_WIDGET (applet->applet), */
/*                                         GTK_STATE_NORMAL, color); */
/*                         break; */

/*                 case PANEL_PIXMAP_BACKGROUND: */
/*                         style = gtk_style_copy (GTK_WIDGET ( */
/* 						applet->applet)->style); */
/*                         if (style->bg_pixmap[GTK_STATE_NORMAL]) */
/*                                 g_object_unref */
/*                                         (style->bg_pixmap[GTK_STATE_NORMAL]); */
/*                         style->bg_pixmap[GTK_STATE_NORMAL] = g_object_ref */
/*                                 (pixmap); */
/*                         gtk_widget_set_style (GTK_WIDGET (applet->applet), */
/*                                         style); */
/*                         g_object_unref (style); */
/*                         break; */

/*                 case PANEL_NO_BACKGROUND: */
/*                 default: */
/*                         break; */
/*         } */

/* } */

static void gpilotd_scroll_to_insert_mark(PilotApplet *applet)
{
	gtk_text_view_scroll_to_mark(
			GTK_TEXT_VIEW(applet->message_area),
       			gtk_text_buffer_get_insert(applet->message_buffer),
			0.2,
			FALSE,
			0.0,
			0.0);
}

static void 
gpilotd_request_completed (GnomePilotClient *client,
			   const gchar *id,
			   unsigned long handle,
			   gpointer user_data)
{
	PilotApplet *applet = PILOT_APPLET (user_data);


	/* could happen, if gpilotd-client is used, for example */
	if (applet->operationDialogWindow == NULL)
	    return;

	gtk_dialog_response (GTK_DIALOG (applet->operationDialogWindow), GTK_RESPONSE_CLOSE);
	if (applet->properties.popups && applet->progressDialog !=NULL) {
		gchar *txt=g_strdup_printf (_("Request %ld has been completed\n"),handle);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);

		gpilotd_scroll_to_insert_mark(applet);
	}

/*	show_message_dialog ("Pilot %s has completed request %ld",
			    strlen (id)==0?"(noname)":id,
			    handle);
*/
}

static void 
gpilotd_conduit_start (GnomePilotClient* client, 
		       const gchar *id, 
		       const gchar *conduit, 
		       const gchar *db_name,
		       gpointer user_data) 
{ 
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog!=NULL) {
		gchar *txt=g_strdup_printf (_("%s Synchronizing : %s"),id, conduit);
		gtk_label_set_text (GTK_LABEL (applet->sync_label),txt);
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->conduit_progress_bar), 0);
		gpilot_applet_progress_start (applet->c_progress);
		g_free (txt);
		txt=g_strdup_printf (_("%s: Started\n"),conduit);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);
		gpilotd_scroll_to_insert_mark(applet);
	}
}

static void 
gpilotd_conduit_end (GnomePilotClient* client, 
		     const gchar *id, 
		     const gchar *conduit,
		     gpointer user_data) 
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog!=NULL) {
		gchar *txt=g_strdup_printf (_("%s Finished : %s"),id, conduit);
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->conduit_progress_bar),1.0);
		gpilot_applet_progress_start (applet->c_progress);
		gtk_label_set_text (GTK_LABEL (applet->sync_label),txt);
		g_free (txt);
		txt=g_strdup_printf (_("%s: Ended\n"),conduit);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (applet->conduit_progress_bar), "");
		gpilotd_scroll_to_insert_mark(applet);
	}
}

static void 
gpilotd_conduit_progress (GnomePilotClient* client, 
			  const gchar *id, 
			  const gchar *conduit, 
			  guint current, 
			  guint total,
			  gpointer user_data)
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	gdouble cur_f = (gdouble)current/(gdouble)total;
	gchar *buf = NULL;
/*
        g_message ("%s : %s : %d/%d = %d%%",id, conduit, current, 
        total,abs (cur_f/(tot_f/100)));
*/
	if (applet->properties.popups && applet->progressDialog!=NULL) {
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->conduit_progress_bar), cur_f); 
		buf = g_strdup_printf (_("%d of %d records"), current, total);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (applet->conduit_progress_bar), buf);
		g_free (buf);
		gpilot_applet_progress_stop (applet->c_progress);
	}
	g_main_context_iteration (NULL, FALSE);
}

static void 
gpilotd_overall_progress (GnomePilotClient* client, 
			  const gchar *id, 
			  guint current, 
			  guint total,
			  gpointer user_data)
{
	PilotApplet *applet = PILOT_APPLET (user_data);

	gdouble cur_f = (gdouble)current/(gdouble)total;

	if (applet->properties.popups && applet->progressDialog!=NULL) {
		gchar *buf=g_strdup_printf (_("Database %d of %d"), current, total);
		gtk_progress_bar_set_text (GTK_PROGRESS_BAR (applet->overall_progress_bar), buf);
		gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (applet->overall_progress_bar),cur_f);
	}
	g_main_context_iteration (NULL, FALSE);
}

static void 
gpilotd_conduit_message (GnomePilotClient* client, 
			 const gchar *id, 
			 const gchar *conduit, 
			 const gchar *message,
			 gpointer user_data) 
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog != NULL) {
		gchar *txt=g_strdup_printf ("%s: %s\n",conduit,message);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);
		gpilotd_scroll_to_insert_mark(applet);
	}
	g_main_context_iteration (NULL, FALSE);
}

static void 
gpilotd_daemon_message (GnomePilotClient* client, 
			const gchar *id, 
			const gchar *conduit,
			const gchar *message,
			gpointer user_data) 
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog != NULL) {
		gchar *txt=g_strdup_printf ("%s\n", message);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);
		gpilotd_scroll_to_insert_mark(applet);
	}
	g_main_context_iteration (NULL, FALSE);
}

static void 
gpilotd_daemon_error (GnomePilotClient* client, 
		      const gchar *id, 
		      const gchar *message,
		      gpointer user_data) 
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog != NULL) {
		gchar *txt=g_strdup_printf ("Error: %s\n", message);
		gtk_text_buffer_insert_at_cursor (applet->message_buffer,txt,-1);
		g_free (txt);
		gpilotd_scroll_to_insert_mark(applet);
	}
}

static void
handle_client_error (PilotApplet *self)
{
	if (self->curstate == SYNCING) {
		show_dialog (self, GTK_MESSAGE_WARNING,
		    _("PDA is currently synchronizing.\nPlease wait for it to finish."));
	} else {
		self->curstate=INITIALISING;
		g_message ("state = INITIALISING");
		gtk_widget_set_tooltip_text(GTK_WIDGET(self->applet),
		    _("Not connected. Please restart daemon."));
		
		pilot_draw (self);
		show_dialog (self, GTK_MESSAGE_ERROR,
		    _("Not connected to gpilotd.\nPlease restart daemon."));
	}
}

static void
about_cb(GtkAction *action, PilotApplet *pilot)
{
	GError *error = NULL;

	GtkWidget *about;
	const gchar *authors[] = {"Vadim Strizhevsky <vadim@optonline.net>",
				  "Eskil Heyn Olsen, <eskil@eskil.dk>",
				  "JP Rosevear <jpr@ximian.com>", 
				  "Chris Toshok <toshok@ximian.com>",
				  "Frank Belew <frb@ximian.com>",
				  "Matt Davey <mcdavey@mrao.cam.ac.uk>",
				  "Halton Huo <haltonhuo@gnome.org>",
				  NULL};

	gtk_window_set_default_icon_from_file (
	    GNOME_ICONDIR "/sync_icon.png", &error);


	if (error != NULL)
	{
	    g_warning ("Error setting icon: " GNOME_ICONDIR "/sync_icon.png: %s", error->message );
	}

	about = gtk_about_dialog_new ();
        gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (about), _("gnome-pilot applet"));
        gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (about), VERSION);
        gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (about), _("Copyright 2000-2006 Free Software Foundation, Inc."));
        gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG (about), _("A PalmOS PDA monitor.\n"));
        gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (about), (const gchar**)authors);
        gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG (about), "Translations by the GNOME Translation Project");
        gtk_about_dialog_set_logo_icon_name (GTK_ABOUT_DIALOG (about), "palm-pilot-sync");

	gtk_window_set_wmclass (
		GTK_WINDOW (about), "pilot", "Pilot");

	/* multi-headed displays... */
	gtk_window_set_screen (GTK_WINDOW (about),
		gtk_widget_get_screen (GTK_WIDGET (pilot->applet)));

	gtk_dialog_run (GTK_DIALOG(about));
	gtk_widget_destroy (about);

	return;
}


static gboolean
exec_on_click_changed_cb (GtkWidget *w, GdkEventFocus *event, gpointer data)
{
	PilotApplet *self = data;
	const gchar * new_list;

	new_list = gtk_entry_get_text (GTK_ENTRY (w));

	if (new_list) {
		g_free (self->properties.exec_when_clicked);
		self->properties.exec_when_clicked = g_strdup (new_list);
	}

	save_properties (self);

	return FALSE;
}

static void
toggle_notices_cb (GtkWidget *toggle, gpointer data)
{
	PilotApplet *self = data;
	self->properties.popups = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON (toggle));
	save_properties (self);
}

static void
properties_cb (GtkAction *action, gpointer user_data)
{
	PilotApplet *self = user_data;
	GtkWidget *button, *entry, *dialog;
	GtkBuilder *ui;
	GError *error = NULL;

	gtk_window_set_default_icon_from_file (GNOME_ICONDIR "/sync_icon.png", &error);
	ui = load_ui (self->ui_file,"PropertiesDialog");
	dialog = get_widget (ui,"PropertiesDialog");
	
	entry = get_widget (ui,"exec_entry");
	if (self->properties.exec_when_clicked)
		gtk_entry_set_text (GTK_ENTRY (entry), self->properties.exec_when_clicked);
	g_signal_connect   (G_OBJECT (entry), "focus-out-event",
			    G_CALLBACK (exec_on_click_changed_cb),
			    self);

  
	button = get_widget (ui,"notices_button");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), self->properties.popups);
	g_signal_connect   (G_OBJECT (button), "toggled",
			    G_CALLBACK (toggle_notices_cb),
			    self);

	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (response_cb),
			  NULL);

	gtk_window_set_screen (GTK_WINDOW (dialog),
		gtk_widget_get_screen (GTK_WIDGET (self->applet)));

	gtk_widget_show_all (dialog);
}

static void
help_cb(GtkAction *action, gpointer user_data)
{
	PilotApplet *applet = user_data;
	GError *error = NULL;

	gtk_show_uri (gtk_widget_get_screen(GTK_WIDGET (applet->applet)),
		      "ghelp:gnome-pilot#pilot-applet",
		      gtk_get_current_event_time (),
		      &error);

	if (error)
	{
		GtkWidget *message_dialog;
		message_dialog = gtk_message_dialog_new (NULL, 0,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("There was an error displaying help: %s"),
							 error->message);
		g_error_free (error);
		gtk_window_set_screen (GTK_WINDOW (message_dialog),
				       gtk_widget_get_screen (GTK_WIDGET (applet->applet)));
		gtk_dialog_run (GTK_DIALOG (message_dialog));
		gtk_widget_destroy (message_dialog);
	}

	return;
}

static void
pref_help_cb(GtkDialog *dialog)
{
	GError *error = NULL;
	gtk_show_uri (gtk_widget_get_screen(GTK_WIDGET (dialog)),
		      "ghelp:gnome-pilot", /* FIXME Add section id later */
		      gtk_get_current_event_time (),
		      &error);

	if (error)
	{
		GtkWidget *message_dialog;
		message_dialog = gtk_message_dialog_new (NULL, 0,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("There was an error displaying help: %s"),
							 error->message);

		g_error_free (error);
		gtk_window_set_screen (GTK_WINDOW (message_dialog),
				       gtk_widget_get_screen (GTK_WIDGET (dialog)));
				       gtk_dialog_run (GTK_DIALOG (message_dialog));
		gtk_widget_destroy (message_dialog);
	}
	return;
}

static void
response_cb (GtkDialog *dialog, gint id, gpointer data)
{

	if (id == GTK_RESPONSE_HELP)
	{
		pref_help_cb (dialog);
		return;
	}

	gtk_widget_hide (GTK_WIDGET (dialog));
}


static void
complete_restore (GnomePilotClient* client, const gchar *id, 
		  unsigned long handle, gpointer user_data)
{	
	PilotApplet *applet = user_data;

	gtk_widget_destroy (applet->operationDialogWindow);
	applet->operationDialogWindow = NULL;
}

static void
restore_cb (GtkAction *action, gpointer user_data)
{
	PilotApplet *applet = user_data;
	int handle,i;
	gchar *pilot_name;
	guint pilot_id;
	GtkBuilder *ui;
	GtkWidget *dir_entry, *id_entry,*frame;
	GtkWidget *device_combo;
	GtkListStore *list_store;
	GtkCellRenderer *renderer;
	GList *list;
	gchar *buf = NULL;
	restore_properties restore_props; 

	
	if (applet->curstate != WAITING) {
		handle_client_error (applet);
		return;
	}
	
	if ((pilot_name=pick_pilot (applet)) == NULL) {
		return;
	}

	if (gnome_pilot_client_get_pilot_base_dir_by_name (applet->gpc,pilot_name,&buf) == GPILOTD_OK) {
		/* XXX backup conduit allows user to modify the backup directory :( */
		restore_props.backupdir = g_strdup_printf ("%s",buf);
		g_free (buf);
	} else {
		handle_client_error (applet);
		return;
	}
	if (gnome_pilot_client_get_pilot_id_by_name (applet->gpc,pilot_name,&pilot_id) != GPILOTD_OK) {
		handle_client_error (applet);
		return;
	}

	ui = load_ui (applet->ui_file, "RestoreDialog");
	applet->restoreDialog = get_widget (ui, "RestoreDialog");

	dir_entry = get_widget (ui, "dir_entry");

	gtk_entry_set_text (GTK_ENTRY (dir_entry),restore_props.backupdir);
	g_free (restore_props.backupdir);
	restore_props.backupdir=NULL;
	/* FIXME: do we need to preserve the backupdir somewhere? */
	
	frame = get_widget (ui,"main_frame");
	buf = g_strdup_printf (_("Restoring %s"), pilot_name);
	gtk_frame_set_label (GTK_FRAME (frame), buf);
	g_free (buf);
	

	id_entry = get_widget (ui, "pilotid_entry");
	
	buf = g_strdup_printf ("%d",pilot_id);
	gtk_entry_set_text (GTK_ENTRY (id_entry),buf);
	g_free (buf);

	applet->properties.cradles=NULL;
	gnome_pilot_client_get_cradles (applet->gpc,&(applet->properties.cradles));
	list = applet->properties.cradles;
	
	device_combo = get_widget (ui, "device_combo");
	list_store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_combo_box_set_model (GTK_COMBO_BOX (device_combo), GTK_TREE_MODEL (list_store));
	renderer = gtk_cell_renderer_text_new();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (device_combo), renderer, TRUE);
	gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (device_combo), renderer, "text", 0);

	i=0;
	while (list){
		GtkTreeIter iter;

		gtk_list_store_append (list_store, &iter);
		gtk_list_store_set (list_store, &iter, 0, (gchar*)list->data, -1);
		list=list->next;
		i++;
	}
	gtk_combo_box_set_active (GTK_COMBO_BOX (device_combo), 0);
	if (i<=1) gtk_widget_set_sensitive (GTK_WIDGET (device_combo),FALSE);
	else  gtk_widget_set_sensitive (GTK_WIDGET (device_combo),TRUE);
	
	if (gtk_dialog_run (GTK_DIALOG (applet->restoreDialog)) == GTK_RESPONSE_OK) {
		int id;
		
		restore_props.backupdir = g_strdup(gtk_entry_get_text (GTK_ENTRY (dir_entry)));
		gtk_widget_destroy (applet->restoreDialog);
		if ( restore_props.backupdir == NULL || 
		     strlen (restore_props.backupdir)==0) {
			show_dialog (applet, GTK_MESSAGE_WARNING,
			    _("No directory to restore from."));
			return;
		}
		
		/* FIXME: how do we specify which device to restore on? */
		id = gnome_pilot_client_connect__completed_request (applet->gpc, complete_restore, applet);
		if (gnome_pilot_client_restore (applet->gpc,pilot_name,
			restore_props.backupdir,
			GNOME_Pilot_IMMEDIATE,0, &handle) == GPILOTD_OK) {
			applet->operationDialogWindow = 
			    gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL,
				GTK_MESSAGE_OTHER, GTK_BUTTONS_CANCEL, "%s",
				_("Press synchronize on the cradle to restore\n" 
				    " or cancel the operation."));
			gint response = gtk_dialog_run(GTK_DIALOG (applet->operationDialogWindow));
			if (applet->operationDialogWindow != NULL)
				gtk_widget_destroy(applet->operationDialogWindow);
			if (response == GTK_RESPONSE_CANCEL) {
				g_message (_("cancelling %d"), handle);
				gnome_pilot_client_remove_request (applet->gpc, handle);
			}
		} else {
			show_dialog (applet, GTK_MESSAGE_WARNING,
			    _("Restore request failed"));
		}
		g_signal_handler_disconnect (applet->gpc, id);		
	} else {
		gtk_widget_destroy (applet->restoreDialog);
	}	
}

static void
state_cb (GtkAction *action, gpointer user_data)
{
	PilotApplet *applet = user_data;
	GnomePilotClient *gpc = applet->gpc;

	if (applet->curstate==WAITING) {
		/* if we're unpaused */ 
		if (gnome_pilot_client_pause_daemon (gpc) != GPILOTD_OK) {		
			handle_client_error (applet);
		}
	}
	else {
		/* if we're paused */
		if (gnome_pilot_client_unpause_daemon (gpc) != GPILOTD_OK) {
			handle_client_error (applet);
		}
	}

	/* FIXME */
	return;
}


static void
restart_cb (GtkAction *action, gpointer user_data)
{
	PilotApplet *applet = user_data;
	GtkWidget *dialog;

	if (applet->curstate == SYNCING) {
		dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
		    GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "%s",
		    _("PDA sync is currently in progress.\nAre you sure you want to restart daemon?"));
		gint response = gtk_dialog_run(GTK_DIALOG(dialog));
		gtk_widget_destroy(dialog);
		if (response != GTK_RESPONSE_YES) {
			return;
		}
		if (applet->progressDialog!=NULL) {
			gtk_widget_hide (applet->progressDialog);
		}
	}
	applet->curstate = INITIALISING;
	g_message ("state = INITIALISING");

	gnome_pilot_client_restart_daemon (applet->gpc);
	gtk_widget_set_tooltip_text(GTK_WIDGET(applet->applet), 
	    _("Trying to connect to the GnomePilot Daemon"));
	applet->timeout_handler_id = g_timeout_add (1000,(GSourceFunc)timeout,applet);

	return;
}

static void
log_cb (GtkAction *action, gpointer user_data)
{
	PilotApplet *applet = user_data;
	
	if (applet->progressDialog != NULL) {
		gtk_label_set_text (GTK_LABEL (applet->sync_label),"Last Sync Log");
		gtk_widget_set_sensitive (applet->cancel_button,TRUE);
		gtk_widget_show_all (applet->progressDialog);
	} else {
		show_dialog (applet, GTK_MESSAGE_INFO,
		    _("There's no last sync on record."));
	}
}

static const GtkActionEntry pilot_applet_menu_actions [] = {
    { "Restore", NULL, N_("_Restore"), NULL, NULL, G_CALLBACK (restore_cb) },
    { "State", GTK_STOCK_INFO, N_("_Pause/Resume"), NULL, NULL, G_CALLBACK (state_cb) },
    { "Restart", NULL, N_("R_estart"), NULL, NULL, G_CALLBACK (restart_cb) },
    { "Log", GTK_STOCK_INFO, N_("_Log"), NULL, NULL, G_CALLBACK (log_cb) },
    { "Props", GTK_STOCK_PROPERTIES, N_("_Props"), NULL, NULL, G_CALLBACK (properties_cb) },
    { "Help", GTK_STOCK_HELP, N_("_Help"), NULL, NULL, G_CALLBACK (help_cb) },
    { "About", GTK_STOCK_ABOUT, N_("_About"), NULL, NULL, G_CALLBACK (about_cb) },
};

static void
install_popup_menu (PilotApplet *self, gboolean on_off)
{
	GtkActionGroup *action_group;        
	char *menu_xml;
	/* ensure the label strings are extracted by intltool.  This list of
	 * strings should match the list in the menu_template below
	 */
#define FOO_STRING _("Restore...") _("Restart") _("Last log...") \
           _("Preferences...") _("Help") _("About")
#undef FOO_STRING
	const char *menu_template = 
		"<menuitem name=\"Restore\" action=\"Restore\" />"
		"<menuitem name=\"Pause/Resume\" action=\"State\" />"
		"<menuitem name=\"Restart\" action=\"Restart\" />"
		"<menuitem name=\"Log\" action=\"Log\" />"
		"<separator/>"
		"<menuitem name=\"Props\" action=\"Props\" />"
		"<menuitem name=\"Help\" action=\"Help\" />"
		"<menuitem name=\"About\" action=\"About\" />";

	if (on_off)
		menu_xml = g_strdup_printf (menu_template, _("Continue"), "refresh");
	else
		menu_xml = g_strdup_printf (menu_template, _("Pause Daemon"), "stop");


	action_group = gtk_action_group_new ("GnomePilotApplet Actions");
	gtk_action_group_set_translation_domain (action_group, GETTEXT_PACKAGE);
	gtk_action_group_add_actions (action_group,
	    pilot_applet_menu_actions,
	    G_N_ELEMENTS (pilot_applet_menu_actions),
	    self);
	panel_applet_setup_menu (PANEL_APPLET (self->applet),
	    menu_xml, action_group);
	g_free (menu_xml);
	g_object_unref (action_group);
}

static void
gpilotd_daemon_pause (GnomePilotClient *client,
		      gboolean on_off,
		      gpointer user_data)
{
	PilotApplet *applet = PILOT_APPLET (user_data);

	if (on_off) {
		if (applet->curstate == WAITING) {
			applet->curstate = PAUSED;
			gtk_widget_set_tooltip_text(GTK_WIDGET(applet->applet), 
			    _("Daemon paused..."));
			g_message ("state = PAUSED");
		}
		else
			handle_client_error (applet);
	}
	else {
		applet->curstate = WAITING;
		gtk_widget_set_tooltip_text(GTK_WIDGET(applet->applet), 
		    _("Ready to synchronize"));
		g_message ("state = READY");
	}

	install_popup_menu (applet, on_off);

	pilot_draw (applet);
}

static void 
gpilotd_conduit_error (GnomePilotClient* client, 
		       const gchar *id, 
		       const gchar *conduit, 
		       const gchar *message,
		       gpointer user_data) 
{
	PilotApplet *applet = PILOT_APPLET (user_data);
	if (applet->properties.popups && applet->progressDialog != NULL) {
		GtkTextIter *iter = NULL;
		gchar *txt=g_strdup_printf ("%s: %s\n",conduit,message);
		gtk_text_buffer_get_end_iter(applet->message_buffer, iter);

		gtk_text_buffer_insert_with_tags_by_name (applet->message_buffer,
							  iter,
							  txt,-1,
							  "foreground-gdk", 
							  applet->errorColor,
							  NULL);
		g_free (txt);
		gtk_text_iter_free (iter);
		gpilotd_scroll_to_insert_mark(applet);
	}
}

/******************************************************************/

static GList*
get_pilot_ids_from_gpilotd (PilotApplet *self) 
{
	GList *pilots=NULL;
	gnome_pilot_client_get_pilots (self->gpc,&pilots);
	return pilots;
}

#if 0
/* not currently used */
static char *
file_type (char *name) 
{
	static char command[128];
	char *tmp;
	FILE *f;

	if (!access (name,R_OK)) return NULL;

	tmp = tmpnam (NULL);
	snprintf (command,127,"file \"%s\" > %s",name,tmp);
	if (system (command)==-1) {
		return NULL;
	}
  
	if ((f = fopen (tmp,"rt"))==NULL) {
		return NULL;
	}

	fgets (command,128,f);
	fclose (f);
	unlink (tmp);

	tmp = (char*)strstr (command,": "); tmp+=2;
	return tmp;
}

static long 
file_size (char *name) 
{
	FILE *FS;
	long ret;

	if ((FS = fopen (name,"rt"))==NULL) {
		return 0;
	}
	fseek (FS,0L,SEEK_END);
	ret = ftell (FS);
	fclose (FS);
	return ret;
}
#endif
/******************************************************************/

static void 
pilot_draw (PilotApplet *self)
{
    if (!gtk_widget_get_realized (GTK_WIDGET (self->image))) {
		g_warning ("pilot_draw ! realized");
		return;
	}

	gtk_image_set_from_file(GTK_IMAGE(self->image), pixmaps[self->curstate]);
  
}

/******************************************************************/

static gboolean 
timeout (PilotApplet *self)
{
	if (self->curstate == INITIALISING) {
		pilot_draw (self);
		
		if (gnome_pilot_client_connect_to_daemon (self->gpc) == GPILOTD_OK) {
			self->curstate = CONNECTING_TO_DAEMON;
			g_message ("state = CONNECTING_TO_DAEMON");
			pilot_draw (self);
		} 
	}
	
	if (self->curstate == CONNECTING_TO_DAEMON) {
		
		if (self->properties.pilot_ids) {
			GList *tmp =self->properties.pilot_ids;
			while (tmp) {
				g_free ((gchar*)tmp->data);
				tmp = g_list_next (tmp);
			} 
			g_list_free (self->properties.pilot_ids);
		}

		self->properties.pilot_ids = get_pilot_ids_from_gpilotd (self);

		if (self->properties.pilot_ids==NULL){
			gtk_widget_set_tooltip_text(GTK_WIDGET(self->applet), 
				_("Not connected. Restart daemon to reconnect"));
			if (self->druid_already_launched == FALSE) {
				self->druid_already_launched = TRUE;
				pilot_execute_program (self, GPILOTD_DRUID);
			}

			self->curstate = INITIALISING;
			g_message ("state = INITIALISING");
			pilot_draw (self);
			/* FIXME: add gpilot_monitor_state_change () */
		} else {
			gtk_widget_set_tooltip_text(GTK_WIDGET(self->applet), 
				_("Ready to synchronize"));
			
			self->curstate = WAITING;
			g_message ("state = READY");
			pilot_draw (self);
		}
	} 

	switch (self->curstate) {
	case WAITING:
		/* this will happen once a sec */
		if (gnome_pilot_client_noop (self->gpc) == GPILOTD_ERR_NOT_CONNECTED) {
			self->curstate = INITIALISING;
			g_message ("state = INITIALISING");
		}
		break;
	case SYNCING: 
	case PAUSED:
	default:
		break;
	}

	/* Keep timeout calls coming */
	return TRUE;
}

static void 
show_dialog (PilotApplet *self, GtkMessageType type, gchar *mesg,...) 
{
	char *tmp;
	va_list ap;

	va_start (ap,mesg);
	tmp = g_strdup_vprintf (mesg,ap);

	self->dialogWindow = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
	    type, GTK_BUTTONS_OK, "%s", tmp);

	gtk_dialog_run (GTK_DIALOG (self->dialogWindow));
	gtk_widget_destroy (self->dialogWindow);
	g_free (tmp);
	va_end (ap);
}


static void
load_properties (PilotApplet *self)
{
	if (self->properties.exec_when_clicked) 
		g_free (self->properties.exec_when_clicked);
  
	self->properties.exec_when_clicked = 
		panel_applet_gconf_get_string ( self->applet,
						"exec_when_clicked",
						NULL);
	self->properties.popups = 
		panel_applet_gconf_get_bool ( self->applet,
					      "pop_ups",
					      NULL);
	/* self->properties.text_limit = */
	/*      panel_applet_gconf_get_int ( self->applet, */
	/*                                   "text_limit", */
	/*                                   &error ); */
}

static void
save_properties (PilotApplet *self)
{
	if (self->properties.exec_when_clicked)
	  panel_applet_gconf_set_string ( self->applet, 
					  "exec_when_clicked", 
					  self->properties.exec_when_clicked, 
					  NULL);
	panel_applet_gconf_set_bool( self->applet,
				     "pop_ups",
				     self->properties.popups,
				     NULL);
}

static void
activate_pilot_menu (GtkComboBox *widget, gpointer data)
{
	PilotApplet *applet = PILOT_APPLET (data);
	gchar *label;
	GtkTreeIter iter;

	gtk_combo_box_get_active_iter(widget, &iter);
	gtk_tree_model_get (gtk_combo_box_get_model (widget), &iter, 0, &label, -1);
	g_object_set_data (G_OBJECT (applet->chooseDialog), "pilot", (gpointer)label);
}

static gchar*
pick_pilot (PilotApplet *self)
{
	gchar * pilot=NULL;
	if (self->properties.pilot_ids) {
		if (g_list_length (self->properties.pilot_ids)==1) {
                        /* there's only one */
			pilot = (gchar*)self->properties.pilot_ids->data;
		} else {
			GList *tmp;
			GtkWidget *combo;
			GtkListStore *list_store;
			GtkCellRenderer *renderer;

			if (self->chooseDialog == NULL) {
				GtkBuilder * ui;

				ui = load_ui (self->ui_file,"ChoosePilot");
				self->chooseDialog = get_widget (ui,"ChoosePilot");
				combo = get_widget (ui, "pilot_combo");
				g_object_set_data (G_OBJECT (self->chooseDialog),"pilot_combo", combo);


			} else {
				combo = g_object_get_data (G_OBJECT (self->chooseDialog), "pilot_combo");
			}

			list_store = gtk_list_store_new (1, G_TYPE_STRING);
			gtk_combo_box_set_model (GTK_COMBO_BOX (combo), GTK_TREE_MODEL (list_store));
			gtk_cell_layout_clear(GTK_CELL_LAYOUT(combo));
			renderer = gtk_cell_renderer_text_new();
			gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo), renderer, TRUE);
			gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (combo), renderer, "text", 0);
			tmp=self->properties.pilot_ids;
			while (tmp!=NULL){
				GtkTreeIter iter;

				gtk_list_store_append (list_store, &iter);
				gtk_list_store_set (list_store, &iter, 0, (gchar*)tmp->data, -1);
				tmp=tmp->next;
			}


			g_signal_connect (G_OBJECT (combo), "changed",
			                  G_CALLBACK (activate_pilot_menu),
			                  self);

			gtk_combo_box_set_active (GTK_COMBO_BOX (combo), 0);
			g_object_set_data (G_OBJECT (self->chooseDialog),"pilot",self->properties.pilot_ids->data);	
			if (gtk_dialog_run(GTK_DIALOG (self->chooseDialog))== 0) {
				pilot=(gchar *)g_object_get_data (G_OBJECT (self->chooseDialog),"pilot");
			} else {
				pilot=NULL;
			}
			gtk_widget_hide(self->chooseDialog);
		}
	}

	return pilot;
}

static void
pilot_execute_program (PilotApplet *self, const gchar *str)
{
	g_return_if_fail (str != NULL);

	if (!g_spawn_command_line_async (str, NULL))
		show_dialog (self, GTK_MESSAGE_WARNING,
		    _("Execution of %s failed"),str);
}

static gint 
pilot_clicked_cb (GtkWidget *widget,
		  GdkEventButton * e, 
		  PilotApplet *self)
{
	if (e->button != 1)
		return FALSE;

	pilot_execute_program (self, self->properties.exec_when_clicked);
	return TRUE; 
}

static void 
cancel_cb (GtkButton* button,gpointer applet)
{
	if (PILOT_APPLET(applet)->progressDialog != NULL) {
		gtk_widget_hide (PILOT_APPLET(applet)->progressDialog);
	}
}


/*
static gint
pilot_timeout (gpointer data)
{
	GtkWidget *pixmap = data;

	gtk_widget_queue_draw (pixmap);
	return TRUE;
}
*/

/* Copied from glib 2.6 so we can work with older GNOME's */
static gchar **
extract_uris (const gchar *uri_list)
{
	GSList *uris, *u;
	const gchar *p, *q;
	gchar **result;
	gint n_uris = 0;

	uris = NULL;

	p = uri_list;

	/* We don't actually try to validate the URI according to RFC
	 * 2396, or even check for allowed characters - we just ignore
	 * comments and trim whitespace off the ends.  We also
	 * allow LF delimination as well as the specified CRLF.
	 *
	 * We do allow comments like specified in RFC 2483.
	 */
	while (p)
	{
		if (*p != '#')
		{
			while (g_ascii_isspace (*p))
				p++;

			q = p;
			while (*q && (*q != '\n') && (*q != '\r'))
				q++;

			if (q > p)
			{
				q--;
				while (q > p && g_ascii_isspace (*q))
					q--;

				if (q > p)
				{
					uris = g_slist_prepend (uris, g_strndup (p, q - p + 1));
					n_uris++;
				}
			}
		}
		p = strchr (p, '\n');
		if (p)
			p++;
	}

	result = g_new (gchar *, n_uris + 1);

	result[n_uris--] = NULL;
	for (u = uris; u; u = u->next)
		result[n_uris--] = u->data;

	g_slist_free (uris);

	return result;
}

static void
dnd_drop_internal (GtkWidget        *widget,
		   GdkDragContext   *context,
		   gint              x,
		   gint              y,
		   GtkSelectionData *selection_data,
		   guint             info,
		   guint             time,
		   gpointer          data)
{
	gchar *pilot_id;
	gint  file_handle;
	PilotApplet *self = PILOT_APPLET(data);
	gchar **names;
	int idx;

	pilot_id=pick_pilot (self);

	switch (info)
	{
	case TARGET_URI_LIST:
	    names = extract_uris ((gchar *) gtk_selection_data_get_data(selection_data));
		for (idx=0; names[idx]; idx++) {
			/*
			  if (strstr (file_type (ptr),"text")) {
			  if (file_size (ptr)>properties.text_limit)
			  g_message ("installing textfile as Doc (not implemented)");
				  else
				  g_message ("installing textfile as Memo (not implemented)");
				  }
			*/
			g_message (_("installing \"%s\" to \"%s\""), names[idx], pilot_id);
			if (pilot_id) {
				gnome_pilot_client_install_file (self->gpc,pilot_id,
								 names[idx],
								 GNOME_Pilot_PERSISTENT,0,&file_handle);
			} 
		}
		g_strfreev (names);
		break;
	default:
		g_warning (_("unknown dnd type"));
		break;
	}
}

static void
applet_destroy (GtkWidget *applet, PilotApplet *self)
{
	g_message (_("destroy gpilot-applet"));

	g_source_remove(self->timeout_handler_id);

	/* XXX free other stuff */
	g_free (self);
}

static void
create_pilot_widgets (GtkWidget *widget, PilotApplet *self) 
{ 
	static GtkTargetEntry drop_types [] = {  
		{ "text/uri-list", 0, TARGET_URI_LIST }, 
	}; 
	static gint n_drop_types = sizeof (drop_types) / sizeof (drop_types[0]); 

	gtk_widget_set_tooltip_text(widget,
		_("Trying to connect to the GnomePilot Daemon"));

	self->c_progress = GPILOT_APPLET_PROGRESS (gpilot_applet_progress_new ()); 

	self->curstate = INITIALISING; 
	g_message ("state = INITIALISING");

	/*	for (i = 0; i < sizeof (pixmaps)/sizeof (pixmaps[0]); i++) 
		pixmaps[i] = gnome_program_locate_file(
		    NULL, GNOME_FILE_DOMAIN_PIXMAP, pixmaps[i], TRUE, NULL);
	*/
	self->image = gtk_image_new_from_file (pixmaps[self->curstate]); 

	g_signal_connect   (G_OBJECT (widget), "button-press-event", 
			    G_CALLBACK (pilot_clicked_cb), self); 


	gtk_widget_show (self->image);   

	gtk_container_add (GTK_CONTAINER (widget), self->image);

	self->timeout_handler_id = g_timeout_add (1000,(GSourceFunc)timeout,self); 

	g_signal_connect   (G_OBJECT (widget),"destroy", 
			    G_CALLBACK (applet_destroy),self); 

	/* g_signal_connect   (G_OBJECT (widget),"change_background",  */
	/* 		    G_CALLBACK (applet_back_change),self);  */

	/* FIXME */
	/* XXX change_orient */
	/* XXX change_size */

	/* Now set the dnd features */
	gtk_drag_dest_set (GTK_WIDGET (self->image), 
			   GTK_DEST_DEFAULT_ALL, 
			   drop_types, n_drop_types, 
			   GDK_ACTION_COPY); 


	g_signal_connect   (G_OBJECT (self->image), 
			    "drag_data_received", 
			    G_CALLBACK (dnd_drop_internal), 
			    self); 

	install_popup_menu (self, FALSE);
}

static gboolean
pilot_applet_fill (PanelApplet *applet)
{
	PilotApplet *self = g_new0 (PilotApplet, 1);

	self->applet = applet;

	self->ui_file="./pilot-applet.ui";
	if (!g_file_test (self->ui_file, G_FILE_TEST_EXISTS)) {
		self->ui_file = g_build_filename (UIDATADIR,
						     "pilot-applet.ui",
						     NULL);
	}
	if (!g_file_test (self->ui_file, G_FILE_TEST_EXISTS)) {
		show_dialog (self, GTK_MESSAGE_ERROR,
		    "Cannot find %s", self->ui_file); /* FIXME: Make translatable after string freeze */
		return -1;
	}

        panel_applet_add_preferences (self->applet, "/schemas/apps/gpilot_applet/prefs", NULL);

	load_properties (self);

	self->gpc = GNOME_PILOT_CLIENT (gnome_pilot_client_new ());
	
	gnome_pilot_client_connect__pilot_connect (self->gpc,
						   gpilotd_connect_cb,
						   self);

	gnome_pilot_client_connect__pilot_disconnect (self->gpc, 
						      gpilotd_disconnect_cb,
						      self);

	gnome_pilot_client_connect__completed_request (self->gpc,
						       gpilotd_request_completed,
						       self);
	
	gnome_pilot_client_connect__start_conduit (self->gpc, 
						   gpilotd_conduit_start,
						   self);

	gnome_pilot_client_connect__end_conduit(self->gpc,
						gpilotd_conduit_end,
						self);


	gnome_pilot_client_connect__progress_conduit (self->gpc, 
						      gpilotd_conduit_progress,
						      self);

	gnome_pilot_client_connect__progress_overall (self->gpc, 
						      gpilotd_overall_progress,
						      self);

	gnome_pilot_client_connect__error_conduit (self->gpc, 
						   gpilotd_conduit_error,
						   self);

	gnome_pilot_client_connect__message_conduit (self->gpc,
						     gpilotd_conduit_message,
						     self);

	gnome_pilot_client_connect__message_daemon (self->gpc,
						    gpilotd_daemon_message,
						    self);

	gnome_pilot_client_connect__error_daemon (self->gpc,
						  gpilotd_daemon_error,
						  self);

	gnome_pilot_client_connect__daemon_pause (self->gpc,
						  gpilotd_daemon_pause,
						  self);

	create_pilot_widgets (GTK_WIDGET (applet), self);

	gtk_widget_show_all(GTK_WIDGET (applet));

	return 0;
}

static gboolean
pilot_applet_factory ( PanelApplet *applet,
		       const gchar *iid,
		       gpointer     data)
{
	gboolean retval = FALSE;
	
	if (!strcmp (iid, "PilotApplet"))
		retval = pilot_applet_fill (applet);

	return retval;
}
PANEL_APPLET_OUT_PROCESS_FACTORY( "PilotAppletFactory",
			     PANEL_TYPE_APPLET,
			     pilot_applet_factory,
			     NULL)
			     
