/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- *//* 
 * Copyright (C) 1998-2000 Free Software Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen
 *
 */

#include "gpilot-gui.h"
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>

static void gpilot_gui_run_dialog(GtkMessageType type, gchar *mesg, va_list ap);

static void
gpilot_gui_run_dialog(GtkMessageType type, gchar *mesg, va_list ap)
{
	char *tmp;
	GtkWidget *dialog;

	tmp = g_strdup_vprintf(mesg,ap);

	dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL, type, GTK_BUTTONS_OK, "%s", tmp);
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
	g_free(tmp);
	va_end(ap);
}

void 
gpilot_gui_warning_dialog (gchar *mesg, ...) 
{
	va_list args;

	va_start (args, mesg);
	gpilot_gui_run_dialog (GTK_MESSAGE_WARNING, mesg, args);
	va_end (args);
}

void 
gpilot_gui_error_dialog (gchar *mesg, ...) 
{
	va_list args;

	va_start (args, mesg);
	gpilot_gui_run_dialog (GTK_MESSAGE_ERROR, mesg, args);
	va_end (args);
}

GPilotPilot* 
gpilot_gui_restore (GPilotContext *context, 
		    GPilotPilot *pilot)
{
	GPilotPilot *result = NULL;
	GtkWidget *d;

	if (pilot) {
		gint response;
		char *tmp;
		tmp = g_strdup_printf ("Restore %s' pilot with id %d\n"
				       "and name `%s'",
				       pilot->pilot_username,
				       pilot->pilot_id, 
				       pilot->name);
		d = gtk_message_dialog_new (NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION,
		    GTK_BUTTONS_YES_NO, "%s", tmp);
		response = gtk_dialog_run (GTK_DIALOG (d));
		g_free (tmp);
		gtk_widget_destroy(d);
		if (response == GTK_RESPONSE_YES) {
			result = pilot;
		} else {
			
		}
	} else {
		gpilot_gui_warning_dialog ("no ident\n"
					   "restoring pilot with ident\n"
					   "exciting things will soon be here...\n");
	}
	return result;
}
