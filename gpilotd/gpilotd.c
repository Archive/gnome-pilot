/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- *//* 
 * Copyright (C) 1998-2000 Free Software Foundation
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Eskil Heyn Olsen
 *          Vadim Strizhevsky
 *          Manish Vachharajani
 *          Dave Camp
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>

#include <glib.h>
#include <glib/gi18n.h>

#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "gpilot-daemon.h"

#define GP_DBUS_NAME         "org.gnome.GnomePilot"

static void remove_pid_file (void);

static void 
sig_hup_handler (int dummy)
{
	signal (SIGHUP, sig_hup_handler);
}

static void 
sig_term_handler (int dummy)
{
	g_message (_("Exiting (caught SIGTERM)..."));
	remove_pid_file ();
	exit (0);
}

static void 
sig_int_handler (int dummy)
{
	g_message (_("Exiting (caught SIGINT)..."));
	remove_pid_file ();
	exit (0);
}

/* This deletes the ~/.gpilotd.pid file */
static void 
remove_pid_file (void)
{
	char *filename;
	
	filename = g_build_filename (g_get_home_dir (), ".gpilotd.pid", NULL);
	unlink (filename);
	g_free (filename);
}

/*
  The creates a ~/.gilotd.pid, containing the pid
   of the gpilotd process, used by clients to send
   SIGHUPS
*/
static void 
write_pid_file (void)
{
	char *filename, *dirname, *buf;
	size_t nwritten = 0;
	ssize_t n, w;
	int fd;
	
	dirname = g_build_filename (g_get_home_dir (), ".gpilotd", NULL);
	if (!g_file_test (dirname, G_FILE_TEST_IS_DIR) && mkdir (dirname, 0777) != 0)
		g_warning (_("Unable to create file installation queue directory"));
	g_free (dirname);
	
	filename = g_build_filename (g_get_home_dir (), ".gpilotd.pid", NULL);
	fd = open (filename, O_CREAT | O_TRUNC | O_RDWR, 0644);
	g_free (filename);
	
	if (fd == -1)
		return;
	
	buf = g_strdup_printf ("%lu", (unsigned long) getpid ());
	n = strlen (buf);
	
	do {
		do {
			w = write (fd, buf + nwritten, n - nwritten);
		} while (w == -1 && errno == EINTR);
		
		if (w == -1)
			break;
		
		nwritten += w;
	} while (nwritten < n);
	
	/* FIXME: what do we do if the write is incomplete? (e.g. nwritten < n)*/
	
	fsync (fd);
	close (fd);
}

/* This function display which pilot-link version was used
   and which features were enabled at compiletime */
static void
dump_build_info (void)
{
	GString *str = g_string_new (NULL);
	g_message ("compiled for pilot-link version %s",
		   GP_PILOT_LINK_VERSION);

	str = g_string_append (str, "compiled with ");
#ifdef WITH_VFS
	str = g_string_append (str, "[VFS] ");
#endif
#ifdef WITH_USB_VISOR
	str = g_string_append (str, "[USB] ");
#endif
#ifdef WITH_IRDA
	str = g_string_append (str, "[IrDA] ");
#endif
#ifdef WITH_NETWORK
	str = g_string_append (str, "[Network] ");
	str = g_string_append (str, "[Bluetooth] ");
#endif
	g_message ("%s", str->str);
	g_string_free (str, TRUE);
}

static DBusGConnection *
get_session_bus (void)
{
        GError          *error;
        DBusGConnection *bus;
        DBusConnection  *connection;
                        
        error = NULL;
        bus = dbus_g_bus_get (DBUS_BUS_SESSION, &error);
        if (bus == NULL) {
                g_warning ("Couldn't connect to system bus: %s",
                           error->message);
                g_error_free (error);
                goto out;
        }

        connection = dbus_g_connection_get_connection (bus);
        dbus_connection_set_exit_on_disconnect (connection, FALSE);

 out:
        return bus;
}

static gboolean
dbus_acquire_name (DBusGConnection *connection)
{
        DBusGProxy *bus_proxy;
        GError     *error;
        guint       result;
        gboolean    ret;

        ret = FALSE;
        bus_proxy = dbus_g_proxy_new_for_name (connection,
                                               DBUS_SERVICE_DBUS,
                                               DBUS_PATH_DBUS,
                                               DBUS_INTERFACE_DBUS);
        if (bus_proxy == NULL) {
                goto out;
        }

        error = NULL;
        if (!dbus_g_proxy_call (bus_proxy,
                                "RequestName",
                                &error,
                                G_TYPE_STRING, GP_DBUS_NAME,
                                G_TYPE_UINT, DBUS_NAME_FLAG_DO_NOT_QUEUE,
                                G_TYPE_INVALID,
                                G_TYPE_UINT, &result,
                                G_TYPE_INVALID)) {
                if (error != NULL) {
                        g_warning ("Failed to acquire %s: %s", GP_DBUS_NAME, error->message);
                        g_error_free (error);
                } else {
                        g_warning ("Failed to acquire %s", GP_DBUS_NAME);
                }
                goto out;
        }

        if (result == DBUS_REQUEST_NAME_REPLY_EXISTS) {
                g_warning (_("gpilotd already running; exiting ...\n"));
                goto out;
        }

        ret = TRUE;

 out:
        return ret;
}

int 
main (int argc, char *argv[])
{
        GMainLoop       *loop;
        GpilotDaemon    *daemon;
        DBusGConnection *connection;
        int              ret;

	/*g_log_set_always_fatal (G_LOG_LEVEL_ERROR |
				G_LOG_LEVEL_CRITICAL |
				G_LOG_LEVEL_WARNING);*/
	
        ret = 1;
	/* Intro */
	g_message ("%s %s starting...", PACKAGE, VERSION);
	dump_build_info ();
	
	/* Setup the correct gpilotd.pid file */
	remove_pid_file ();
	write_pid_file ();
	
	g_type_init ();

        connection = get_session_bus ();
        if (connection == NULL) {
                goto out;
        }

        daemon = gpilot_daemon_new ();
        if (daemon == NULL) {
                goto out;
        }

        if (! dbus_acquire_name (connection) ) {
                g_warning ("Could not acquire name; bailing out");
                goto out;
        }

	gtk_init (&argc, &argv);

        loop = g_main_loop_new (NULL, FALSE);

	signal (SIGTERM, sig_term_handler);
	signal (SIGINT, sig_int_handler);
	signal (SIGHUP, sig_hup_handler);
	
        g_main_loop_run (loop);

        if (daemon != NULL) {
                g_object_unref (daemon);
        }

	/* It is unlikely that we will end here */
	remove_pid_file ();
	
	ret = 0;
 out:
        return ret;
}
